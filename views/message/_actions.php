<?php

/* @var $this \yii\web\View */

use thyseus\message\models\Message;
use yii\helpers\Html;

$action = Yii::$app->controller->action->id;

?>
<p>
    <div class="text-right">
        <div class="inbox-button-holder">
            <?php
            if ($action == 'compose') {
                $caption = '<i class="fas fa-edit"></i> <strong>' . Yii::t('message', 'Compose') . '</strong>';
            } else {
                $caption = '<i class="fas fa-edit"></i> ' . Yii::t('message', 'Compose');
            } ?>

            <?= Html::a($caption, ['compose'], ['class' => 'btn btn-primary']) ?>
            <div class="btn-group">
                <?php

                $icon = '<i class="fas fa-inbox"></i> ';
                $caption = Yii::t('message', 'Inbox');

                if ($action == 'inbox') {
                    $caption = Html::tag('strong', $caption);
                }

                $inbox_count = Message::find()->where([
                    'to' => Yii::$app->user->id,
                    'status' => [
                        Message::STATUS_READ,
                        Message::STATUS_UNREAD,
                    ]])->count(); 
                ?>

                <?= Html::a(sprintf('%s %s (%d)', $icon, $caption, $inbox_count),
                    ['inbox'],
                    ['class' => 'btn btn-primary']) 
                ?>    


                <?php
                $icon = '<i class="fas fa-share-square"></i> ';
                $caption = Yii::t('message', 'Sent');

                if ($action == 'sent') {
                    $caption = Html::tag('strong', $caption);
                }

                $sent_count = Message::find()->where([
                    'from' => Yii::$app->user->id,
                    'status' => [
                        Message::STATUS_READ,
                        Message::STATUS_UNREAD,
                    ]
                ])->count(); 
                ?>

                <?= Html::a(sprintf('%s %s (%d)', $icon, $caption, $sent_count),
                    ['sent'],
                    ['class' => 'btn btn-primary']) 
                ?>

                <?php
                $icon = '<i class="fab fa-firstdraft"></i> ';
                $caption = Yii::t('message', 'Drafts');

                if ($action == 'drafts') {
                    $caption = Html::tag('strong', $caption);
                }

                $draft_count = Message::find()->where([
                    'from' => Yii::$app->user->id,
                    'status' => Message::STATUS_DRAFT,
                ])->count(); 
                ?>

                <?= Html::a(sprintf('%s %s (%d)', $icon, $caption, $draft_count),
                    ['drafts'],
                    ['class' => 'btn btn-primary']) 
                ?>

                <?php
                $icon = '<i class="fas fa-clone"></i> ';
                $caption = Yii::t('message', 'Templates');

                if ($action == 'templates') {
                    $caption = Html::tag('strong', $caption);
                }

                $template_count = Message::find()->where([
                    'from' => Yii::$app->user->id,
                    'status' => Message::STATUS_TEMPLATE,
                ])->count(); 
                ?>

                <?= Html::a(sprintf('%s %s (%d)', $icon, $caption, $template_count),
                    ['templates'],
                    ['class' => 'btn btn-primary']) 
                ?>


                <div class="btn-group">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        <?= Yii::t('message', 'More'); ?>&nbsp;<span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <?php if ($action == 'inbox') { ?>
                            <li> <?= Html::a(
                                '<i class="fas fa-eye"></i> ' . Yii::t('message', 'Mark all messages as read'),
                                ['mark-all-as-read']) 
                                ?>
                            </li>
                        <?php } ?>

                        <?php
                        if ($action == 'manage-draft' && !isset($_GET['hash'])) {
                            $caption = '<i class="fa fa-plus-circle"></i> <strong>' . Yii::t('message', 'Create a draft') . '</strong>';
                        } else {
                            $caption = '<i class="fa fa-plus-circle"></i> ' . Yii::t('message', 'Create a draft');
                        } ?>

                        <li> <?= Html::a($caption, ['manage-draft']) ?> </li>
                        <?php
                        if ($action == 'manage-template' && !isset($_GET['hash'])) {
                            $caption = '<i class="fa fa-plus-circle"></i> <strong>' . Yii::t('message', 'Create a template') . '</strong>';
                        } else {
                            $caption = '<i class="fa fa-plus-circle"></i> ' . Yii::t('message', 'Create a template');
                        } ?>

                        <li> <?= Html::a($caption, ['manage-template']) ?> </li>

                        <?php
                        if ($action == 'signature') {
                            $caption = '<i class="fas fa-signature"></i> <strong>' . Yii::t('message', 'Signature') . '</strong>';
                        } else {
                            $caption = '<i class="fas fa-signature"></i> ' . Yii::t('message', 'Signature');
                        } ?>

                        <li> <?= Html::a($caption, ['signature']) ?> </li>

                        <?php
                        if ($action == 'out-of-office') {
                            $caption = '<i class="far fa-calendar-times"></i> <strong>' . Yii::t('message', 'Out of Office Message') . '</strong>';
                        } else {
                            $caption = '<i class="far fa-calendar-times"></i> ' . Yii::t('message', 'Out of Office Message');
                        } ?>

                        <li> <?= Html::a($caption, ['out-of-office']) ?> </li>

                        <?php
                        if ($action == 'ignorelist') {
                            $caption = '<i class="fas fa-ban"></i> <strong>' . Yii::t('message', 'Ignorelist') . '</strong>';
                        } else {
                            $caption = '<i class="fas fa-ban"></i> ' . Yii::t('message', 'Ignorelist');
                        } ?>

                        <li> <?= Html::a($caption, ['ignorelist']) ?> </li>                          
                    </ul>
                </div>
            </div>
        </div>
    </div>
</p>